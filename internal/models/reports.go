package models

import (
  "gorm.io/gorm"
)

type Report struct {
  gorm.Model
  ID          string `json: "id"`
  Content       string `json: "content"`
}
